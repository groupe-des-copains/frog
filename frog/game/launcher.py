import pygame
import sys
import random

from pygame.locals import *
from frog.game.states import Menu, Game, Pause, End

class Launcher():
    def __init__(self, screen, data_manager):
        self.screen = screen
        pygame.display.set_caption('Frog')
        self.state = "menu"
        self.data_manager = data_manager
        self.clock = pygame.time.Clock()
        self.tick_cp=0
    # self.last_tick = pygame.time.get_ticks()
        self.screen_res = [1280, 720]
        self.clock.tick(60)
        self.reset()

        while 1:
            self.loop()

    def reset(self):
        self.menu = Menu(self.data_manager, self.screen_res)
        self.game = Game(self.data_manager, self.screen_res)
        self.pause = Pause(self.data_manager, self.screen_res)
        self.end = End(self.data_manager, self.screen_res)

    def get_active(self):
        if self.state == "menu":
            return self.menu         
        if self.state == "game":
            return self.game   
        if self.state == "pause":
            return self.pause      
        if self.state == "end":
            return self.end         
    
    def loop(self):
        # main game loop
        self.tick()
        if self.state != self.get_active().state:
            if self.get_active().state == "menu":
                self.reset()
                self.state = "menu"
            
            self.state = self.get_active().state
            self.get_active().state = self.state

        self.game.release_keys()
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            else:
                self.get_active().manage_event(event)
        
            

        self.get_active().draw(self.screen)

        self.show_fps()
        pygame.display.update() 

    def show_fps(self):
        font = pygame.font.SysFont("Arial", 18) # TODO: a voir ce qu'on fait de cette font
        fps = str(self.ttime)
        fps_text = font.render(fps, 1, pygame.Color("coral"))
        self.screen.blit(fps_text, (10,0))

    def tick(self):
        self.ttime = pygame.time.Clock().tick(160)
        #pygame.time.Clock().tick_busy_loop(1) #Stabilise?
        self.mpos = pygame.mouse.get_pos()
        self.keys_pressed = pygame.key.get_pressed()