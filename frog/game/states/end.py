import pygame
from frog.engine.hud import NavState, Text

class End(NavState):
    def __init__(self, data_manager, screen_res):
        super().__init__(data_manager, screen_res, "end")
        print("init end")

    def init_texts(self):
        self.texts.append(Text("End", self.font, "red", "center", "mid"))

    def manage_event(self, event):
        # the main event loop, detects keypresses
        if event.type == pygame.locals.KEYDOWN:
            if event.key == pygame.locals.K_SPACE:
                self.state = "menu"
    
  
