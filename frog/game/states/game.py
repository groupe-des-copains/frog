import pygame
from frog.engine.state import State
from frog.engine.player import Player
from frog.engine.world import World

class Game(State):
    def __init__(self, data_manager, screen_res):
        super().__init__(data_manager, screen_res, "game")
        print("init game")

        self.data_manager = data_manager



        

        self.entities = self.gen_entities()
        self.world = World(data_manager,self.entities, self.player)

    def gen_entities(self):
   
        all_entities = pygame.sprite.Group()
        player = Player(self.data_manager, 0, 0)
       
        self.player = player

        entities = pygame.sprite.Group(player)
        entities.add(all_entities)
        
        return entities


    def manage_event(self, event):
        # the main event loop, detects keypresses
        # TODO: a voir pour faire plus propre quand on aura plus de visu
        if event.type == pygame.locals.KEYDOWN:
            if event.key == pygame.locals.K_SPACE:
                self.state = "end"
            elif event.key == pygame.locals.K_ESCAPE:
                self.state = "pause"
            # else:
                # self.player.manage_event(event)
        self.world.manage_event(event)


    def release_keys(self):
        self.world.release_keys()
        

    def do_points(self):
        pass

    def draw(self, screen):
        self.world.draw(screen)
        self.world.routine()
        
        
    
        