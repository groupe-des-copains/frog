import pygame
from frog.engine.hud import NavState, Text

class Pause(NavState):
    def __init__(self, data_manager, screen_res):
        super().__init__(data_manager, screen_res, "pause")
        self.font = pygame.font.SysFont("Impact", 55)
        self.alpha_bg = (123, 123, 190, 20)

    
    def init_texts(self):
        self.texts.append(Text("Pause", self.font, "white", "center", "mid"))

    def manage_event(self, event):
        # the main event loop, detects keypresses
        # TODO: a voir pour faire plus propre quand on aura plus de visu
        if event.type == pygame.locals.KEYDOWN:
            if event.key == pygame.locals.K_SPACE:
                self.state = "game"