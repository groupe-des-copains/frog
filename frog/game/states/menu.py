import pygame

from frog.engine.hud.nav_state import NavState

class Menu(NavState):
    def __init__(self, data_manager, screen_res):
        super().__init__(data_manager, screen_res, "menu")
        self.bg = (123, 123, 190)
        
    def manage_event(self, event):
        # the main event loop, detects keypresses
        if event.type == pygame.locals.KEYDOWN:
            if event.key == pygame.locals.K_SPACE:
                self.state = "game"
    