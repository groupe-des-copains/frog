class State():
    def __init__(self, data_manager, screen_res, name):
        self.state = name
        self.data_manager = data_manager
        self.screen_res = screen_res

    def manage_event(self, event):
        print("manage_event", self.state)
        # self.screen_res = (pygame.display.Info().current_w, pygame.display.Info().current_h)

    def draw(self, screen):
        print("draw ", self.state)
