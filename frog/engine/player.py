import pygame
from pygame.locals import *

from frog.engine.character import Character

initial_jump_speed = 4
clock = pygame.time.Clock()


class Player(Character):

    def __init__(self, data_manager, x, y):
        self.data_manager = data_manager
        super().__init__(self.data_manager.characters["player"], x, y)
        self.name = "player"
        # mock pour debug

        self.keys_pressed = {}
        self.keys_released = {}
        self.wall_jump_time = 0

        self.max_dx = 20
        self.max_dy = -15
        self.jump_time = 0
        
        self.jump_cp = 0

        self.start_wall_jump = True
        self.wall_jump = False

        self.load_jump = False
        self.cant_jump = False
        self.jump_phase = 0
        self.jump_phase1 = 100
        self.jump_phase2 = 700 
        self.max_jump_phase = 1500

        self.keys_pressed = {}
        self.keys_released = {}

        self.inertie_value = 0.90
        self.go_inertie = 0
     
        self.max_double_jump = 2
        for i in [K_DOWN, K_UP, K_LEFT, K_RIGHT, K_SPACE, K_LSHIFT]:
            self.keys_pressed[i] = False
            self.keys_released[i] = False

        self.rect.topleft = (1000,150)

    def manage_event(self, event):
        
        if hasattr(event, 'key'):
            if event.key not in [K_DOWN, K_UP, K_LEFT, K_RIGHT, K_LSHIFT]:
                return
        
            if event.type == KEYDOWN:
                self.keys_pressed[event.key] = True
                self.keys_released[event.key] = False
                

            elif event.type == KEYUP:
                self.keys_pressed[event.key] = False
                self.keys_released[event.key] = True
        
                
                  

    def after_wall_jump(self):
        self.jump_cp = 0
        self.wall_jump = False
        self.wall_jump_time = 0
        self.start_wall_jump = True
        self.wall_jump = False
        self.is_jump = True
   
        if self.hiting_wall_side:
            self.dx = -4
        else:
            self.dx = 4

    def jump(self):
        self.is_jump = True

        facteur = 1 
        if self.jump_time > self.jump_phase2:
            facteur = 1.5
        if self.jump_time > self.max_jump_phase :
            facteur = 2.2
        
        self.dy = -5 * facteur
        
        if facteur != 1:
            if self.direction:
                self.dx = 15
            else:
                self.dx = -15


        if self.dy < self.max_dy:
            self.dy = self.max_dy
        self.jump_time = 0
        self.load_jump = False

               


    def update_position(self, obstacles):

        dt = clock.tick()
        self.tick_cp += 5
        etats = [self.etat]

        

        if self.is_on_ground:
            self.dy = 3

        if self.etat != 3:
            self.go_inertie = 0

        if self.keys_pressed[K_DOWN]:
            pass

        elif self.keys_pressed[K_LEFT]:
            self.go_inertie = 0
            if not self.wall_jump:           
                self.dx -= 0.1
                self.direction = False
                if self.dx < -self.max_dx:
                    self.dx = -self.max_dx

        elif self.keys_pressed[K_RIGHT]:
            self.go_inertie = 0
            if not self.wall_jump:
                self.dx += 0.1
                self.direction = True
                if self.dx > self.max_dx:
                    self.dx = self.max_dx
        else:
            self.go_inertie = self.dx
            

        if self.keys_pressed[K_LSHIFT]:
            if self.is_on_ground:
                self.max_dx = 20
        else:
            self.max_dx = 10

        if self.keys_pressed[K_UP] and not self.cant_jump:
            self.jump_time += dt
            if self.jump_cp < self.max_double_jump:
                self.load_jump = True
            
                if self.keys_pressed[K_RIGHT] or self.keys_pressed[K_LEFT]:
                    if self.jump_time > 100:
                        self.dx*=self.inertie_value
                        
                if self.jump_time > self.jump_phase1:
                    self.jump_phase = 1

                if self.jump_time > self.jump_phase2  and self.jump_time <= self.max_jump_phase :
                    self.jump_phase = 2
                  
                    self.dy = 1.5
                    self.dx=0

                elif self.jump_time > self.max_jump_phase :
                    self.load_jump = False
                        
                    self.dy = 5
                    self.jump_cp = self.max_double_jump
                    self.cant_jump = True
        

            if self.wall_jump:
                if self.wall_jump_time > 200:
                    self.dy = -7
                    self.after_wall_jump()


        if self.keys_released[K_UP] == True and self.jump_cp < self.max_double_jump:
            self.jump_cp += 1
            self.cant_jump = False
            self.keys_released[K_UP] = False

            if not self.is_on_ground and self.jump_time < 700:
                pass
            else:
                self.jump()
            



        self.etat = 3 # stop
        
        if self.wall_jump:
            self.etat = 4 # wall_jump
    
            if self.start_wall_jump == True:
                self.wall_jump_time = 0
                dt = clock.tick()
                self.start_wall_jump = False

            self.wall_jump_time += dt

            if self.wall_jump_time < 400: # combien de temps on reste sur le mur
                self.dy = 0
                self.dx = 0
            else:
                self.after_wall_jump()
                self.dy = 3

        if self.is_on_ground and self.dx!=0:
            self.etat = 0 # run

        if not self.load_jump and not self.is_collide:
            if self.dy > 0 :
                self.etat = 1 # fall
            elif self.dy < 0 :
                self.etat = 1 # jump
        if self.jump_phase != 0:
            if self.jump_phase == 1:
                self.etat = 4 # jump_hold
            if self.jump_phase == 2:
                self.etat = 3 # jump_load
            self.jump_phase = 0

        etats.append(self.etat)
        if etats[0] != etats[1]:
            self.image_x = 0

        # print("x: ", self.rect[0], " y: ", self.rect[1], " self.wall_jump_time: ", self.wall_jump_time," dy: ", self.dy, " dx: ", self.dx, "self.wall_jump ", self.wall_jump)
