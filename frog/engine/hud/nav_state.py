import pygame
from frog.engine.state import State
from frog.engine.hud import Text

class NavState(State):
    def __init__(self, data_manager, screen_res, name):
        super().__init__(data_manager, screen_res, name)
        self.bg = "white"
        self.alpha_bg = (0, 0, 0, 0)
        self.font = pygame.font.SysFont("Impact", 55)
        self.texts = []
        self.init_texts()

    def draw_bg(self, screen):
        screen.fill(self.bg)

    def draw_alpha_bg(self, screen):
        screen.fill(self.alpha_bg)

    def init_texts(self):
        self.texts.append(Text("Play", self.font, "white", "center", "mid"))

    def draw(self, screen):
        if self.bg != "white":
            self.draw_bg(screen)
        else:
            self.draw_alpha_bg(screen)
        for text in self.texts:
            self.draw_text(screen, text)

    def draw_text(self, screen, text):
        text_object = text.font.render(text.text, 1, text.color)
        last_text = self.texts[-1]

        if text.x == "center":
            text_x = (pygame.display.Info().current_w / 2) - (text_object.get_width() / 2)
        elif text.x == "left":
            text_x = text_object.get_width() / 2
        elif text.x == "right":
            text_x = (pygame.display.Info().current_w) - (text_object.get_width() *2.5)
        elif text.x == "next":
            text_x = last_text.get_width() + text.next_x
        else:
            text_x = text.x

        if text.y == "top":
            text_y =  text_object.get_height() / 2
        elif text.y == "mid":
            text_y = (pygame.display.Info().current_h - (text_object.get_height())) / 2
        elif text.y == "bottom":
            text_y = (pygame.display.Info().current_h - (text_object.get_height())*2)
        elif text.y == "next":
            text_y = last_text.get_height() + text.next_y
        else:
            text_y = text.y
        screen.blit(text_object, (text_x, text_y))
