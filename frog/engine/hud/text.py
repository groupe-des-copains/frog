class Text():
    def __init__(self, text, font, color, x, y, next_x = 0, next_y = 0):
        self.text = text
        self.font = font
        self.color = color
        self.x = x
        self.y = y
        self.next_x = next_x
        self.next_y = next_y
        