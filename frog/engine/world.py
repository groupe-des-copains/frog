import pygame
from frog.engine.physics import collision
from frog.engine.player import Player
from frog.engine.level import LevelManager
from frog.engine.camera import Camera

class World():
    def __init__(self, data_manager, entities, player):
        self.player = player
        self.data_manager = data_manager
        self.entities = entities
        self.obstacles = pygame.sprite.Group()
        self.level_manager = LevelManager(self.data_manager)
        self.camera = Camera(self.level_manager.rect_map_config, (1280, 720))
        self.obstacles.add(self.level_manager.get_entities())
        self.solids = pygame.sprite.Group()
        self.solids.add(self.entities)
        self.solids.add(self.obstacles)
        


    def draw(self, screen):
        self.draw_background(screen)
        self.camera.update_camera(self.player)

        for layer in self.level_manager.layers:
            screen.blit(layer.image, self.camera.apply_layer(layer))
        for solid in self.solids:
            screen.blit(solid.image, self.camera.apply(solid))


        
    
    def draw_background(self, screen):
        screen.fill((123, 123, 190)) 

    def manage_event(self, events):
        for entity in self.entities:
            entity.manage_event(events)

    def update_position(self):
        for entity in self.entities:
            entity.update_position(self.obstacles)

    def manage_collision(self):
        for entity in self.entities:
            collision.get_position(entity, self.obstacles)

    def routine(self):
        self.update_position()
        self.manage_collision()
        self.animate_entities()
    
    def animate_entities(self):
        for entity in self.entities:
            entity.animate_def(entity.etats[entity.etat])
    
    def release_keys(self):
        for entity in self.entities:
            for i in entity.keys_released:
                entity.keys_released[i] = False
