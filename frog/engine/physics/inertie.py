import math

def inertie(entity, dx):
    if dx > -0.1 and dx < 0.1:
        dx = 0
    
    if dx < 0 :
        return dx*entity.inertie_value
    elif dx > 0: 
        return dx*entity.inertie_value
    else:
        return dx
