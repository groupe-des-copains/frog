import math
from frog.engine.physics import inertie



def gravity(entity, coord_x, coord_y, dy, dx, c):
        g = entity.constante_gravite
        new_x = 0
        new_y = 0

        if dy < 0 and dy > -0.9999:
            dy = 0.0001
        
        if entity.go_inertie != 0:
            delta_x = inertie.inertie(entity,entity.go_inertie)
            entity.dx = delta_x
            entity.go_inertie = entity.dx
        else:
            delta_x = dx

        v0 = math.sqrt(dy**2)

        if dy < 0:
            delta_y = -(1/2)*-g-v0
            vitesse = abs(-g+v0)
            entity.dy = vitesse*-1

        elif dy > 0:
            delta_y = -(1/2)*g+v0
            vitesse = abs(-g-v0)
            entity.dy = vitesse
        else:
            delta_y = 0

        if dx == 0 and dy == 0:
            delta_x = 0
            delta_y = 0

        new_y += delta_y
        if dx < 0:
            new_x = delta_x
        else:
            new_x = delta_x

        return [new_x, new_y]