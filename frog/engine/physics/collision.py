from frog.engine.physics import gravite
import pygame

def collision_check(entity, rect, obstacles):
    hit_list = []
    for tile in obstacles:
        if rect.colliderect(tile):
            if not isinstance(tile, str):
                hit_list.append(tile)
            else:
                entity.coliding_item = True

    return hit_list

def get_position(entity, obstacles):
    entity.collision_way = {"top": False, "down": False,
                            "left": False, "right": False}
    

    

    coords = gravite.gravity(entity, entity.rect[0], entity.rect[1], entity.dy, entity.dx, 2)
    
    mvt_y = entity.rect[1]+coords[1]-entity.rect[1]
    entity.rect[1] += coords[1]
    rect_temp = compute_hitboxes(entity, entity.rect)



    hit_list=collision_check(entity, rect_temp, obstacles)

    while hit_list != []:
        rect_temp = compute_hitboxes(entity, entity.rect)
        hit_list = collision_check(entity, rect_temp, obstacles)
     
        if mvt_y > 0:
            entity.rect.bottom -=1
            entity.collision_way["down"] = True

        else:
            entity.rect.top += 1
            entity.collision_way["top"] = True


    
    entity.rect[0] += coords[0]
    rect_temp = compute_hitboxes(entity, entity.rect)





    hit_list=collision_check(entity,rect_temp, obstacles)

    while hit_list != []:

        rect_temp = compute_hitboxes(entity, entity.rect)
        
        hit_list = collision_check(entity,rect_temp, obstacles)
        if entity.dx > 0:
            entity.rect.right -= 1
            entity.collision_way["right"] = True
        else:
            entity.rect.left += 1
            entity.collision_way["left"] = True

    entity.is_on_ground = False

    if entity.collision_way["down"] == True:
        entity.is_on_ground = True
        entity.is_jump = False
        entity.jump_cp = 0

    if entity.collision_way["top"] == True:
        entity.dy =2
        

    if (entity.collision_way["left"] or entity.collision_way["right"]) and entity.collision_way["down"] == False:

        entity.wall_jump = True
        entity.is_jump = False
        
        if entity.collision_way["right"] == True:
            entity.hiting_wall_side = True
        else:
            entity.hiting_wall_side = False
    if (entity.collision_way["left"] or entity.collision_way["right"]):
        entity.dx = 0


    entity.is_collide = False
    for i in entity.collision_way:
        if entity.collision_way[i]:
            entity.is_collide = True
    
    


    
    if entity.collision_way["top"]==True:
        print(entity.collision_way)

def compute_hitboxes(entity, rect):

        etat_string = entity.etats[entity.etat] + entity.get_direction()
        hitbox = entity.sheet.hitboxes[etat_string]

        true_rect_left = rect.left+hitbox[0]
        true_rect_top = rect.top+hitbox[1]
        true_rect_width = hitbox[2]
        true_rect_height = hitbox[3]

        true_rect = pygame.Rect(true_rect_left,true_rect_top,true_rect_width,true_rect_height)
      
        return true_rect
       
 
