import pygame
import math
import copy

class Spritesheet():
    def __init__(self, params):
        self.name = params['name']
        self.format = params['format']
        self.filename = params['filename']
        self.nb_img_v = params['nb_img_v']
        self.nb_img_h = params['nb_img_h']
        self.width_img = params['img_width']
        self.height_img = params['img_height']
        self.sens_base_h = params["sens_base_h"]
        self.sens_base_v = params["sens_base_v"]

        if self.nb_img_h > 0 and self.width_img > 0:
            self.width_spritesheet = self.nb_img_h * self.width_img
        else:
            self.width_spritesheet = params['width_spritesheet']

        if self.nb_img_v > 0 and self.height_img > 0:
            self.height_spritesheet = self.nb_img_v * self.height_img
        else:
            self.height_spritesheet = params['height_spritesheet']

        self.vertical_flip = params['vertical_flip']
        self.horizontal_flip = params['horizontal_flip']
        self.animations = params['animations']
        self.scale_x = params['scale_x']
        self.scale_y = params['scale_y']
        self.color_key = params['color_key']
        self.type = params['type']

        # self.resolution_x = 1280
        # self.resolution_y = 720

        self.etats = []
        self.config_layers = {}
        self.images = {}
        self.hitboxes = {}


        #     self.width_img = math.floor(self.width_img/(self.scale_x * -1))
        # else:
        #     self.width_img = self.width_img * self.scale_x

        #     self.height_img = math.floor(self.height_img / (self.scale_y * -1))
        # else:
        #     self.height_img = self.height_img * self.scale_y

        try:
            self.sheet = pygame.image.load(self.filename).convert_alpha()
            self.sheet = pygame.transform.scale(
                self.sheet, (math.floor(self.width_spritesheet*self.scale_x), math.floor(self.height_spritesheet*self.scale_y)))
            self.load()

        except pygame.error:
            print('Unable to load spritesheet image:', self.filename)
            raise SystemExit

    # Load a specific image from a specific rectangle

        # Load a specific image from a specific rectangle
    def image_at(self, pos, width_img=0, height_img=0):
        if not width_img:
            width_img = self.width_img
        if not height_img:
            height_img = self.height_img

        rect = pygame.Rect(
            (pos[0], pos[1], width_img, height_img))

        image = pygame.Surface(rect.size).convert_alpha()
        image.fill((0, 0, 0, 0))

        image.blit(self.sheet, (0, 0), rect)
        if self.color_key is not None:
            if self.color_key == -1:
                self.color_key = image.get_at((0, 0))
            image.set_colorkey(self.color_key, pygame.RLEACCEL)
        return image


    def scale_with_screen(self, size):
        # A VOIR UNE PROCHAINE FOIS SUIVANT CE QU'ON VEUT
        print("[scale_with_screen] ", size, self.filename)
        # coeff_resolution_x = (size[0]/self.resolution_x)
        # coeff_resolution_y = (size[1]/self.resolution_y)
        # self.width_img = math.floor(self.width_img*coeff_resolution_x)
        # self.height_img = math.floor(self.height_img*coeff_resolution_y)
        # self.resolution_x = size[0]
        # self.resolution_y = size[1]
        # for i in self.images:
        #     # print("[self.images[i]]", self.images[i])
        #     # print("[size]", size)
        #     images = []

        #     print(self.width_img, self.height_img)

        #     for image in self.images[i]:
        #         print("[image]", image)

        #         image = pygame.transform.scale(image, (self.width_img, self.height_img))
        #         # image = pygame.transform.flip(image, True, False)
        #         images.append(image)
        #     self.images[i] = images

    def load(self):
        if self.format == "simple":
            self.load_simple()
        else:
            self.load_particular()

        for i in self.horizontal_flip:
            surface_list = []
            for j in self.images[i]:
                surface_list.append(pygame.transform.flip(j, True, False))


            if self.sens_base_h == "left":
                sens_two = "_right"
                sens_base = "_left"
            else:
                sens_two = "_left"
                sens_base = "_right"
            self.images[i + sens_two] = surface_list
            self.images[i + sens_base] = self.images.pop(i)
            
            if len(self.hitboxes) > 0:
                self.hitboxes[i + sens_two] = self.hitboxes[i]
                self.hitboxes[i + sens_base] = self.hitboxes.pop(i)
            
            elif self.type == "layers":
                self.config_layers[i + sens_two] = self.config_layers[i]
                self.config_layers[i + sens_base] = self.config_layers.pop(i)

        for bool_sens in range(2):
            side = ""
            if bool_sens == 0:
                side = "_left"
            elif bool_sens == 1:
                side = "_right"
            for i in self.vertical_flip:
                name = i
                if i in self.horizontal_flip:
                    name += side
                surface_list = []

                for j in self.images[name]:
                    surface_list.append(pygame.transform.flip(j, False, True))

                self.images[name + "_flip_v"] = surface_list
                self.images[name] = self.images.pop(name) # a voir ce qu'on en fait

                if len(self.hitboxes) > 0:
                    print("[wtf] on a des hitboxes inversées ??")
                    # self.hitboxes[i] = self.hitboxes[name]
                    # self.hitboxes[i + "_flip_v"] = self.hitboxes.pop(i)

                elif self.type == "layers": # des layers ne peux pas avoir de hitboxes
                    self.config_layers[name + "_flip_v"] = copy.copy(self.config_layers[name]) # a voir ce que vaut copy
                    if self.config_layers[name + "_flip_v"]["position"] == "up":
                        self.config_layers[name + "_flip_v"]["position"] = "down"
                    elif self.config_layers[name + "_flip_v"]["position"] == "down":
                        self.config_layers[name + "_flip_v"]["position"] = "up"

                    
                    
    # Ouais j'ai pas d'idées de nom
    def load_particular(self):
        x, y = 0, 0
        for animation in self.animations:
            animation["height"] = math.floor(animation["height"])
            animation["width"] = math.floor(animation["width"])
            self.images[animation["name"]] = []
        
            if self.type == "layers":
                self.config_layers[animation["name"]] = {}
                self.config_layers[animation["name"]]["name"]=animation["name"]
                self.config_layers[animation["name"]]["perspective"]=animation["perspective"]
                self.config_layers[animation["name"]]["frequence"]=animation["frequence"]
                self.config_layers[animation["name"]]["position"]=animation["position"]
            else:
                print(animation["hitboxes"])
                if animation["hitboxes"]:
                    self.hitboxes[animation["name"]] = pygame.Rect(
                        animation["hitboxes"])

            for j in range(animation["nb_img_v"]):
                for i in range(animation["nb_img_h"]):
                    x = animation["start_x"] + i * \
                        (animation["skip_x"]+animation["width"])
                    y = animation["start_y"] + j * \
                        (animation["skip_y"]+animation["height"])
                    self.images[animation["name"]].append(self.image_at(
                        (x*self.scale_x, y*self.scale_y), animation["width"]*self.scale_x, animation["height"]*self.scale_y))

    def load_simple(self):
        x, y = 0, 0
        for i in self.animations:
            have_hitboxes = len(i) > 2
            if have_hitboxes:
                self.hitboxes[i[0]] = pygame.Rect(i[2], i[3], i[4], i[5])
            if i[0] == 'skip':
                x += i[1]
                if x >= self.nb_img_h:
                    y += x // self.nb_img_h
                    x = x % self.nb_img_h
                continue

            tmp_array = []
            if i[0] in self.images.keys():
                tmp_array = self.images[i[0]]

            self.images[i[0]] = tmp_array

            for j in range(i[1]):
                self.images[i[0]].append(self.image_at(
                    (x*self.width_img, y*self.height_img)))
                x += 1
                if x >= self.nb_img_h:
                    x = 0
                    y += 1

    def __getitem__(self, key):
        return self.images[key]

    def __setitem__(self, key, item):
        self.images[key] = item
