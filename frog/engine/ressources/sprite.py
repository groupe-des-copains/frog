import pygame

class Sprite(pygame.sprite.Sprite):
    def __init__(self, sheet, x, y, etat = ""):
        self.x = x
        self.y = y
        self.sheet = sheet
        self.etat = etat
        if len(self.etat) <= 0:
            for i in self.sheet.images:
                self.etat = i
                break
        self.init_images()
        self.image_x = 0
        self.image_y = 0
        self.tick_cp = 0

        self.tick_anim = 50

        self.direction = True

        pygame.sprite.Sprite.__init__(self)


    def get_direction(self):
        if self.direction:
            return "_right"
        return "_left"

    def animate_def(self,anim):
        tick_anim = self.sheet.animations[0]['animation_speed']
        if self.tick_cp > tick_anim:
            self.image = self.sheet[anim+self.get_direction()][self.image_x]
            self.image_x += 1

            if self.image_x >= len(self.sheet[anim+self.get_direction()]):
                self.image_x = 0

            self.tick_cp = 0

    def init_images(self):
        if self.sheet != 0:
            self.image = self.sheet.images[self.etat][0]
            self.rect = self.image.get_rect()
            self.rect.topleft = (self.x, self.y)
