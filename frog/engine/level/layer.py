import pygame

class Layer(pygame.sprite.Sprite):
    def __init__(self, sheets, config_params, index_layer):
        self.name = config_params["name"]
        self.index = config_params["index"]
        self.perspective = config_params["perspective"]
        self.rect = pygame.Rect(config_params["x"], config_params["y"], config_params["width"], config_params["height"])
        self.x = config_params["x"]
        self.image = pygame.Surface(self.rect.size).convert_alpha()
        self.image.fill((0, 0, 0, 0))
        self.image.blit(sheets[self.name], (0, 0), self.rect)
        self.index_layer = index_layer
        pygame.sprite.Sprite.__init__(self)