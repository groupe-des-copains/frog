import pygame
from frog.engine.ressources import Sprite
class Tile(Sprite):
    
    def __init__(self, rect, tileset, etat):
        x = rect[0]*rect[2]
        # print(rect[1])
        # print(rect[3])
        y = rect[1]*rect[3] 
        self.etat = etat
        self.index = 0
        self.sheet = tileset
        self.init_image()
        # print("[x]", x)
        # print("[y]", y)
        super().__init__(tileset, x, y, self.etat)

    def init_image(self):
        # Si l'image est vide pas besoin d'image
        if self.etat == 0:
            # JUSTE POUR DEBUG
            self.etat = "empty"

        if self.etat == "empty":
            return