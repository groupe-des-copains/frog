
import random
import pygame
from frog.engine.level import Tile, Layer, Map

class LevelManager():
    def __init__(self, data_manager):
        self.data_manager = data_manager
        self.tilesets = []
        self.layers = []
        self.init_tilesets()
        self.case_width = 128
        self.case_height = 128        
        self.current_layer = 0
        self.init_level("level_one")

    def init_level(self, level_name):
        self.level = self.data_manager.levels[level_name]
        self.next_level = self.data_manager.levels[self.level["next_level"]]
        self.map = Map(self.level["map"], self.data_manager, self.tilesets)
        self.rect_map_config = self.get_rect_map_config()
        self.init_layers()

    def init_tilesets(self):
        for t in self.data_manager.tilesets:
            self.tilesets.append(t)

    def init_layers(self):
        for layer in self.level["layers"]:
            config_layer = self.level["layers"][layer]
            config_layer["name"] = layer 
            layer_to_add = Layer(self.data_manager.layers, config_layer, self.current_layer)
        self.layers.append(layer_to_add)

        for layer in self.next_level["layers"]:
            config_layer = self.next_level["layers"][layer]
            config_layer["name"] = layer 
            layer_to_add = Layer(self.data_manager.layers, config_layer, self.current_layer+1)
        self.layers.append(layer_to_add)


    def get_rect_map_config(self):
        return pygame.Rect(self.map.width, self.map.height, self.case_width, self.case_height)

    def get_entities(self):
        entities = pygame.sprite.Group()
        for y in range(self.map.height):
            for x in range(self.map.width):
                case = self.map.to_use[y][x]
                if case.etat and case.etat != "empty":
                    entities.add(case)

        return entities