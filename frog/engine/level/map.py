import pygame
from frog.engine.level import Tile

class Map():
    
    def __init__(self, map_file, data_manager, tilesets):
        print("Init Map")
        print(map_file)
        print("------------------------")
        self.case_width = 128
        self.case_height = 128
        self.tilesets = tilesets
        self.data_manager = data_manager
        self.data = map_file
        self.width = len(self.data[0])
        self.height = len(self.data)
        self.to_use = []
        self.filled = self.init_fill()
        self.compute_map()

    def compute_map(self):
        for y in range(self.height):
            line = []
            for x in range(self.width):
                line.append(self.get_case(x+1, y+1))
            self.to_use.append(line)

    def get_case(self, x, y):
        if not self.filled[y][x]:
            return Tile((x, y, self.case_width, self.case_height), 0, "empty")

        else:
            indice_tileset = self.filled[y][x]-1
            tileset = self.data_manager.tilesets[self.tilesets[indice_tileset]]
            name_sprite = ""
            bloc_right = False
            bloc_left = False
            bloc_up = False
            bloc_down = False

            if not self.filled[y][x-1]:
                name_sprite += "n"
                bloc_left = True
            name_sprite += "left_"

            if not self.filled[y][x+1]:
                name_sprite += "n"
                bloc_right = True
            name_sprite += "right_"

            if not self.filled[y-1][x]:
                name_sprite += "n"
                bloc_up = True
            name_sprite += "up_"

            if not self.filled[y+1][x]:
                name_sprite += "n"
                bloc_down = True
            name_sprite += "down_"

            if bloc_left or bloc_up or not self.filled[y-1][x-1]:
                name_sprite += "n"
            name_sprite += "up_left_"

            if bloc_right or bloc_up or not self.filled[y-1][x+1]:
                name_sprite += "n"
            name_sprite += "up_right_"

            if bloc_left or bloc_down or not self.filled[y+1][x-1]:
                name_sprite += "n"
            name_sprite += "down_left_"

            if bloc_right or bloc_down or not self.filled[y+1][x+1]:
                name_sprite += "n"
            name_sprite += "down_right"
        
        return Tile((x-1, y-1, self.case_width, self.case_height), tileset, name_sprite)

    def init_fill(self):
        bloc = []
        line = []

        # Pour x = 0
        for tmp_x in range(self.width+2):
            line.append(1)
        bloc.append(line)

        for y in range(self.height):
            line = [1]
            for x in range(self.width):
                line.append(self.data[y][x])
            line.append(1)
            bloc.append(line)

        # Pour x = max
        line = []
        for tmp_x in range(self.width+2):
            line.append(1)
        bloc.append(line)
        return bloc

# Continuer de rajouter des méthodes et du métier ici pour que les maps soient autonomes