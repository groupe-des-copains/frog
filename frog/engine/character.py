import pygame
from frog.engine.ressources import Sprite
class Character(Sprite):
    
    def __init__(self, sheet, x, y):
        super().__init__(sheet, x, y)
        self.dx = 0
        self.dy = 1
        self.is_on_ground = False
        self.is_jump = False
        self.is_collide = False
        self.constante_gravite = 0.1
        self.etats = ["run", "jump", "dash", "stop", "wall_jump","hold_jump","load_jump"]

