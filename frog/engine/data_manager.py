from frog.engine.ressources.sample import *
from frog.engine.ressources.spritesheet import *
import json
import re

audio_dir = ["hero"]

class DataManager():
    def __init__(self, config):
        self.config = config
        self.path = config["path"]
        self.path_config = config["path_config"]
        self.game_name = config["game_name"]

        if self.config["sound_path"]:
            self.sounds = self.load_samples([])
        if self.config["characters_path"]:
            self.characters = self.load_characters(["player.png"])
        if self.config["tilesets_path"]:
            self.tilesets = self.load_tilesets(["tileset", "tubes", "toyo", "cage", "wallset"])
        if self.config["levels_path"]:
            self.levels = self.load_levels(["level_one", "level_two"])
        if self.config["layers_path"]:
            self.layers = self.load_layers(["layer.png", "layer2.png"])

    def load_levels(self, levels):
        path = self.path + self.config["levels_path"]
        all_levels = {}
        for level in levels:
            with open(self.path+"levels/"+level + ".json") as level_file:
                level_params = json.load(level_file)
                all_levels[level] = level_params

        return all_levels

    def load_tilesets(self, tilesets):
        path = self.path
        path_config = path + self.path_config
        path_tilesets = path + "assets/tilesets/"
        sheets = {}
        for tileset in tilesets:
            sheet_params = {}
            with open(path_config+tileset + ".json") as sheet_file:
                sheet_params = json.load(sheet_file)
                sheet_params['filename'] = path_tilesets + tileset + ".png"

            sheets[tileset] = Spritesheet(sheet_params)

        return sheets

    def load_samples(self, audios):
        samples_t = {}
        sound_path = self.path+self.config["sound_path"]
        #print(sound_path)
        for folder_name in audios:
            samples_y = {}
            for file_name in os.listdir(sound_path+folder_name):
                samples_y[file_name.replace(".wav", "")] = load_sound(
                    sound_path+folder_name+"/"+file_name)
            samples_t[folder_name] = samples_y
        return samples_t

    def load_characters(self, characters):

        path_config = self.path + self.path_config
        path_spritesets = self.path + self.config["characters_path"]
        sheets = {}
        for character in characters:
            sheet_params = {}
            extension = re.sub("^.*\.", "", character)
            name = character.replace("."+extension, "")

            with open(path_config+name + ".json") as sheet_file:
                sheet_params = json.load(sheet_file)
                sheet_params['filename'] = path_spritesets + character

            sheets[name] = Spritesheet(sheet_params)

        return sheets

    def load_layers(self, layers):

        path_config = self.path + self.path_config
        path_spritesets = self.path + self.config["layers_path"]
        sheets = {}
        for layer in layers:
            extension = re.sub("^.*\.", "", layer)
            name = layer.replace("."+extension, "")
            sheets[name] = pygame.image.load(path_spritesets+layer).convert_alpha()

        return sheets
