import pygame
from pygame.locals import *
import random
import math


class Camera(object):
    def __init__(self, rect_map_config, screen_config):

        # Initialisation de la camera avec les dimensions de la map en pixels
        # Ainsi que la résolution

        # self.data_manager = data_manager
        self.rect_map_config = rect_map_config
        self.screen_config = screen_config

        self.width_map = rect_map_config[0] * rect_map_config[2]
        self.height_map = rect_map_config[1] * rect_map_config[3]
        self.layers = []
        self.state = pygame.Rect(0, 0, self.width_map, self.height_map)
        
    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def apply_layer(self, target):
        index = target.index
        index = math.floor(math.sqrt(index)*index)
        to_add = target.index_layer *1280
        x = (self.state.topleft[0]+target.x)/(index)+to_add
        y = self.state.topleft[1]/(index)

        return target.rect.move(x, y)

    def update_camera(self, target):
        # Pour que le player soit centré
        self.state = self.camera_func(target.rect)

    def camera_func(self, target_rect):
        max_x = self.state.width-self.screen_config[0]
        max_y = self.state.height-self.screen_config[1]

        x = target_rect[0]
        y = target_rect[1]
        x = -x + (self.screen_config[0]/2)
        y = -y + (self.screen_config[1]/2)

        x = min(0, x)          # stop scroll à gauche
        x = max(-(max_x), x)   # stop scroll à droite
        y = max(-(max_y), y)   # stop scroll en bas
        y = min(0, y)          # stop scroll en haut

        rect = pygame.Rect(x, y, self.state.width, self.state.height)
        return rect
            
