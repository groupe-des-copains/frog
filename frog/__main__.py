import pygame
import sys
from frog.engine.data_manager import DataManager
from frog.game import Launcher

audio = ["hero"]
config_data = {
    "game_name": "frog",
    "path" : "frog/data/",
    "sound_path": "audio/",
    "layers_path": "assets/layers/",
    "characters_path": "assets/characters/",
    "tilesets_path": "tilesets/",
    "obstacles_path": "obstacles/",
    "levels_path": "maps/",
    "path_config": "config_sprites/"
}


screen_res = [1280, 720]
screen = pygame.display.set_mode(screen_res, pygame.HWSURFACE| pygame.DOUBLEBUF | pygame.RESIZABLE | pygame.SCALED)
pygame.init()


if __name__ == '__main__':
    data_manager = DataManager(config_data)
    

    if len(sys.argv) == 1:
        Launcher(screen, data_manager)
        
    elif sys.argv[1] == "edit":
        print("edit")
        # Editor(screen, data_manager)
