# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

try:
    long_description = open("README.md").read()
except IOError:
    long_description = ""

setup(
    name="frog",
    version="0.1.0",
    description="Frog Rebirth of Green",
    license="MIT",
    url='https://framagit.org/groupe-des-copains/frog',
    packages=find_packages(where='frog'),
    author="groupe-des-copains",
    author_email="lel@lel",
    install_requires=['pygame', 'json'],
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.9",
    ],
    entry_points={
    'console_scripts': [
        'Weed=Weed.cli:main',
    ]}
)
